﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CustomButton : MonoBehaviour
{
    public UnityEvent OnButtonPressed;

    [Header("Components")] public MeshRenderer meshrenderer;
    public AudioSource audioSource;

    [Header("Materials")] public Material activeMaterial;
    public Material inactiveMaterial;

    private AudioClip buttonPressedActive;
    private AudioClip buttonPressedInactive;

    private bool isBeingPressed;

    private Vector3 buttonStartPostion;


    private void Start()
    {
        buttonStartPostion = transform.localPosition;
        buttonPressedActive = GameManager.instance.audioGlobal.button_pressedActive;
        buttonPressedInactive = GameManager.instance.audioGlobal.button_pressedInactive;

        GetComponent<CustomInteractable>().onHoverBegin += InitiateButtonPress;
    }

    private bool _activated = true;

    public bool activated
    {
        get { return _activated; }
        set
        {
            _activated = value;
            if (activated)
            {
                meshrenderer.material = activeMaterial;
            }
            else
            {
                meshrenderer.material = inactiveMaterial;
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        InitiateButtonPress();
    }

    public void InitiateButtonPress()
    {
        StartCoroutine(ButtonPress());
    }

    private IEnumerator ButtonPress()
    {
        if (!isBeingPressed)
        {
            isBeingPressed = true;

            if (activated)
            {
                OnButtonPressed.Invoke();

                audioSource.PlayOneShot(buttonPressedInactive);
                audioSource.PlayOneShot(buttonPressedActive);
            }
            else
            {
                audioSource.PlayOneShot(buttonPressedInactive);
            }

            yield return AnimateButton();
            isBeingPressed = false;
        }
    }


    private IEnumerator AnimateButton()
    {
        float moveSpeed = 2;
        float buttonMoveDistance = .2f;

        // Keep a note of the time the movement started.
        var startTime = Time.time;

        var buttonEndPostion = buttonStartPostion;
        buttonEndPostion.y -= buttonMoveDistance;
        // Calculate the journey length.
        var journeyLength = Vector3.Distance(buttonStartPostion, buttonEndPostion);

        //animate down
        yield return TransformAnimationLoop(startTime, moveSpeed, buttonStartPostion, buttonEndPostion, journeyLength);
        //animate up
        startTime = Time.time;
        yield return TransformAnimationLoop(startTime, moveSpeed, buttonEndPostion, buttonStartPostion, journeyLength);
    }

    private IEnumerator TransformAnimationLoop(float startTime, float moveSpeed, Vector3 startPosition,
        Vector3 endPosition, float journeyLength)
    {
        while (transform.localPosition != endPosition)
        {
            // Distance moved = time * speed.
            float distCovered = (Time.time - startTime) * moveSpeed;

            // Fraction of journey completed = current distance divided by total distance.
            float fracJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.
            transform.localPosition = Vector3.Lerp(startPosition, endPosition, fracJourney);

            //wait one frame
            yield return 0;
        }
    }
}