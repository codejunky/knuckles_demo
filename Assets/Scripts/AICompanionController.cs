﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions.Must;
using UnityEngine.Events;

public class AICompanionController : MonoBehaviour
{
    private string EYE_RADIUS_STRING = "Vector1_2FED53FA";

    private Material eyeMaterial;
    private int EYE_RADIUS_ID;
    private float radius = 0.5f;
    private float[] clipSampleData;
    private AudioSource audioSource;
    private Transform playerTransform;
    private Transform lookAtTransform;

    public Transform parentTransform;
    [SerializeField]
    private GameObject eye;

    public int sampleDataLength = 32;
    public float loudnessIntensity = 1;
    [SerializeField]
    private bool updateLookAt = true;

    internal Action<Collision> OnAiCollisionEnter;

    private void Awake()
    {
        clipSampleData = new float[sampleDataLength];
        eyeMaterial = eye.GetComponent<Renderer>().material;
        audioSource = eye.GetComponent<AudioSource>();
        EYE_RADIUS_ID = Shader.PropertyToID(EYE_RADIUS_STRING);
    }

    private void Start()
    {
        playerTransform = Camera.main.transform;
        lookAtTransform = playerTransform;
    }


    private void FixedUpdate()
    {
        if (audioSource.isPlaying)
        {
            //stolen from: https://answers.unity.com/questions/1167177/how-do-i-get-the-current-volume-level-amplitude-of.html
            audioSource.clip.GetData(clipSampleData, audioSource.timeSamples);

            var clipLoudness = 0f;
            foreach (var sample in clipSampleData)
            {
                clipLoudness += Mathf.Abs(sample);
            }

            clipLoudness /= sampleDataLength;

            eyeMaterial.SetFloat(EYE_RADIUS_ID, clipLoudness * loudnessIntensity + radius);
        }

        if (updateLookAt)
        {
            transform.LookAt(lookAtTransform);
        }

    }

    private void OnCollisionEnter(Collision other)
    {
        if(OnAiCollisionEnter != null)
            OnAiCollisionEnter.Invoke(other);
    }

    public IEnumerator TurnTowards(Transform targetTransform, float turnSpeed = 0.1f, bool keepLooking = false)
    {
        updateLookAt = false; //make sure look at doesn't change on every fixed update
        
        var startTime = Time.time;
        var lookAtPos = lookAtTransform.position;
        var startPosition = lookAtPos;
        var targetPos = targetTransform.position;

        while (lookAtPos != targetPos)
        {
            var journeyLength = Vector3.Distance(startPosition, targetPos);
        
            float distCovered = (Time.time - startTime) * turnSpeed;

            float fracJourney = distCovered / journeyLength;

            lookAtPos = Vector3.Lerp(startPosition, targetPos, fracJourney);
            transform.LookAt(lookAtPos);
            
            yield return null;
        }

        if (keepLooking)
        {
            LookAt(targetTransform);
        }

    }

    private void LookAt(Transform lookAtTransform)
    {
        updateLookAt = true;
        this.lookAtTransform = lookAtTransform;
    }

    public IEnumerator MoveTo(Vector3 targetPos, float movementSpeed = 1)
    {
        var startPosition = parentTransform.position;
        var startTime = Time.time;
        var journeyLength = Vector3.Distance(startPosition, targetPos);


        while (parentTransform.position != targetPos)
        {
            // Distance moved = time * speed.
            float distCovered = (Time.time - startTime) * movementSpeed;

            // Fraction of journey completed = current distance divided by total distance.
            float fracJourney = distCovered / journeyLength;

            // Set our position as a fraction of the distance between the markers.
            parentTransform.position = Vector3.Lerp(startPosition, targetPos, fracJourney);

            yield return null;
        }
    }
}