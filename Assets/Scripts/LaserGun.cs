﻿using System;
using System.Collections;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;
using Random = UnityEngine.Random;

public class LaserGun : CustomThrowable
{
    [Header("Laser Gun Specific")] public GameObject firePoint;
    public ParticleSystem particleSystem;

    public Color laserColor;
    public float laserWidth = 0.02f;
    public float shotDisplayTime = 0.2f;
    public float cooldownTime = 0.5f;

    private AudioSource audioSource;
    private LineRenderer laser;
    private float timeSinceLastShot = 0f;

    private AudioClip laserGunShotSound;

    public Action OnShotInHead;
    public Action OnShotAI;

    private ParticleSystem.EmitParams emitParams;


    protected override void Awake()
    {
        base.Awake();
        
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = GameManager.instance.audioGlobal.sheep_laserGunShot;
        laser = GetComponent<LineRenderer>();

        if (laser == null)
        {
            laser = firePoint.AddComponent<LineRenderer>();
        }

        if (laser.sharedMaterial == null)
        {
            laser.material = new Material(Shader.Find("Unlit/Color"));
            laser.material.SetColor("_Color", laserColor);
            Helpers.LineRendererSetColor(laser, laserColor, laserColor);
            Helpers.LineRendererSetWidth(laser, laserWidth, laserWidth);
        }

        laser.useWorldSpace = true;


        particleSystem.transform.parent = null;
        
        SteamVR_Actions.default_TriggerPressed.AddOnStateDownListener(OnTriggerPressed, SteamVR_Input_Sources.LeftHand);
        SteamVR_Actions.default_TriggerPressed.AddOnStateDownListener(OnTriggerPressed, SteamVR_Input_Sources.RightHand);
    }

    void OnTriggerPressed(SteamVR_Action_Boolean action, SteamVR_Input_Sources inputSources)
    {
        var triggerPressed = interactable.attachedToHand?.handType == inputSources;

        if (triggerPressed && Time.time - timeSinceLastShot > cooldownTime)
            StartCoroutine(Shoot());
    }

    IEnumerator Shoot()
    {
        audioSource.pitch = Random.Range(0.5f, 1.5f);
        audioSource.Play();

        laser.enabled = true;
        timeSinceLastShot = Time.time;
        laser.material.SetColor("_Color", laserColor);
        Helpers.LineRendererSetColor(laser, laserColor, laserColor);
        Helpers.LineRendererSetWidth(laser, laserWidth, laserWidth);

        RaycastHit hitInfo;
        bool hit = Physics.Raycast(firePoint.transform.position, firePoint.transform.forward, out hitInfo, 1000);
        Vector3 endPoint;

        if (hit == true)
        {
            endPoint = hitInfo.point;
            laserHit(hitInfo, firePoint.transform.forward);
        }
        else
        {
            endPoint = firePoint.transform.position + (firePoint.transform.forward * 1000f);
        }


        laser.SetPositions(new Vector3[] {firePoint.transform.position, endPoint});
        
        yield return new WaitForSeconds(shotDisplayTime);
        
        laser.enabled = false;
    }

    private void laserHit(RaycastHit hit, Vector3 direction)
    {
        Transform trans = hit.transform;
        var sheep = trans.GetComponentInParent<SheepController>();
        if (sheep != null)
        {
            sheep.OnHit();
        }
        else if (trans.GetComponent<CustomHead>() != null)
        {
            OnShotInHead?.Invoke();
        }
        else if (trans.GetComponent<AICompanionController>() != null)
        {
            OnShotAI?.Invoke();
        }
        else if (hit.rigidbody != null)
        {
            hit.rigidbody.AddForceAtPosition(direction * 100, hit.point);
        }
        else
        {
            var button = trans.GetComponent<CustomButton>();
            if (button != null)
            {
                button.InitiateButtonPress();
            }
        }

        //emitParams.position = hit.point;
        //particleSystem.Emit(emitParams, 20);
        particleSystem.Stop();
        particleSystem.transform.position = hit.point;
        particleSystem.Play();
//        particleSystem.Emit();
    }

    public void Shred()
    {
        gameObject.SetActive(false);
    }
}