﻿using System.Collections;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.SceneManagement;
using Valve.VR.InteractionSystem;

public class SceneController_06 : MonoBehaviour
{
    public SceneStates_06 sceneStates;
    public Audio_06 sceneAudio;
    public AudioSource aIAudioSource;
    public LaserGun[] laserGuns;
    public AICompanionController aiCompanion;
    public Transform shredderLookAtTransform;
    public Transform shredderMoveToTransform;
    public CustomButton aiButton;

    [Header("Options")] public bool skipIntro;

    private AudioGlobal audioGlobal;

    private bool importantSequenceActive = false;

    private void Awake()
    {
        laserGuns = FindObjectsOfType<LaserGun>();
        laserGuns.ForEach(gun => gun.OnShotAI += OnShotAi);
        foreach (var gun in laserGuns)
        {
            gun.OnShotAI += OnShotAi;
            gun.OnShotInHead += OnShotInHead;
        }
        aiCompanion.OnAiCollisionEnter += OnAiCollisionEnter;
    }

    void Start()
    {
        audioGlobal = GameManager.instance.audioGlobal;
        StartCoroutine(Play01Intro());
    }

    IEnumerator Play01Intro()
    {
        importantSequenceActive = true;

        yield return new WaitForSeconds(2);

        if (!GameManager.instance.statesGlobal.SuicideReaction)
        {
            if(!skipIntro)
                yield return aIAudioSource.PlayClipAndWait(sceneAudio.prisonCell01GoodMorning);
        }
        else
            yield return SuicideReactions();

        importantSequenceActive = false;

    }

    public void OnShredderBroken()
    {
        StartCoroutine(Play02ShredderBroken());
    }

    IEnumerator Play02ShredderBroken()
    {
        importantSequenceActive = true;

        aiButton.transform.parent.gameObject.SetActive(true);
        aiButton.activated = false;
        
        yield return new WaitForSeconds(1);
        yield return aIAudioSource.PlayClipAndWait(sceneAudio.prisonCell10ShredderBroken);
        
        StartCoroutine(aiCompanion.TurnTowards(shredderLookAtTransform, 2, keepLooking: true));
        yield return aiCompanion.MoveTo(shredderMoveToTransform.position);
        aiButton.activated = true;
        print("reached location!");
        
        importantSequenceActive = false;
    }
    
    private void OnShotInHead()
    {
        GameManager.instance.statesGlobal.SuicideReaction = true;
        SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);
    }
    
    IEnumerator SuicideReactions()
    {
        switch (GameManager.instance.statesGlobal.SuicideReactionCounter)
        {
            case 0:
                yield return aIAudioSource.PlayClipAndWait(audioGlobal.sheepShooter00aSuicide1);
                print("suicide1");
                break;
            case 1:
                yield return aIAudioSource.PlayClipAndWait(audioGlobal.sheepShooter00aSuicide2);
                print("suicide2");
                break;
            default:
                yield return aIAudioSource.PlayClipAndWait(audioGlobal.sheepShooter00aSuicide3);
                GameManager.instance.statesGlobal.SuicideReactionCounter = -1;
                print("suicide3");
                break;
        }

        GameManager.instance.statesGlobal.SuicideReactionCounter++;
        GameManager.instance.statesGlobal.SuicideReaction = false;
    }
    
    private void OnShotAi()
    {
        StartCoroutine(AiShotReactions());
    }
    
    private int aiShotCounter = 0;
    private bool reactingToBeingShot = false;
    private IEnumerator AiShotReactions()
    {
        if(reactingToBeingShot) yield break;

        reactingToBeingShot = true;
        switch (aiShotCounter)
        {
            case 0: 
                yield return Speak(audioGlobal.sheepShooter00bShotAi1);
                print("ai shot 1");
                break;
            case 1:
                yield return Speak(audioGlobal.sheepShooter00bShotAi2);
                print("ai shot 2");
                break;
            default:
                yield return Speak(audioGlobal.sheepShooter00bShotAi3);
                print("ai shot 3");
                aiShotCounter = -1;
                break;
        }
        aiShotCounter++;
        reactingToBeingShot = false;
    }
    
    private void OnAiCollisionEnter(Collision other)
    {
        if (other.transform.GetComponent<Eatable>() != null)
        {
            StartCoroutine(FoodThrownReaction());
        }
        else if (other.transform.GetComponent<CustomThrowable>() != null)
        {
            StartCoroutine(StuffThrownReaction());
        }
    }
    
    private int foodThrownReactionCounter = 0;
    private bool foodThrownReactionActive = false;
    IEnumerator FoodThrownReaction()
    {
        if (foodThrownReactionActive) yield break;

        foodThrownReactionActive = true;

        switch (foodThrownReactionCounter)
        {
            case 0:
                yield return Speak(audioGlobal.Eating00aFoodThrown1);
                break;
            case 1:
                yield return Speak(audioGlobal.Eating00aFoodThrown2);
                break;
            default:
                yield return Speak(audioGlobal.Eating00aFoodThrown3);
                foodThrownReactionCounter = -1;
                break;
        }

        foodThrownReactionCounter++;
        foodThrownReactionActive = false;
    }
    
    private int stuffThrownReactionCounter = 0;
    private bool stuffThrownReactionActive = false;
    IEnumerator StuffThrownReaction()
    {
        if (stuffThrownReactionActive) yield break;

        stuffThrownReactionActive = true;

        switch (stuffThrownReactionCounter)
        {
            case 0:
                yield return Speak(audioGlobal.player00astuffThrown1);
                break;
            case 1:
                yield return Speak(audioGlobal.player00astuffThrown2);
                break;
            default:
                yield return Speak(audioGlobal.player00astuffThrown3);
                stuffThrownReactionCounter = -1;
                break;
        }

        stuffThrownReactionCounter++;
        stuffThrownReactionActive = false;
    }

    IEnumerator Speak(AudioClip audioClip)
    {
        if (importantSequenceActive) yield break;

        yield return aIAudioSource.PlayClipAndWait(audioClip);
    }

    public void EndLevel()
    {
        importantSequenceActive = true;
        
        SceneManager.LoadScene(7);
    }


}