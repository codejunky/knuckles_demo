﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_06 : MonoBehaviour
{
    public AudioClip prisonCell01GoodMorning;
    public AudioClip prisonCell10ShredderBroken;
    [Header("Intermission")] public AudioClip Intermission4;

}
