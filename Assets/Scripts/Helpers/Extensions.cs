using System.Collections;
using UnityEngine;

namespace DefaultNamespace
{
    public static class Extensions
    {
        public static IEnumerator PlayClipAndWait(this AudioSource audioSource, AudioClip clip)
        {
            audioSource.clip = clip;
            audioSource.Play();
            yield return new WaitForSeconds(clip.length);
        }
    }
}