﻿using UnityEngine;

public class Helpers
{
    public static void LineRendererSetColor(LineRenderer lineRenderer, Color startColor, Color endColor)
    {
        lineRenderer.startColor = startColor;
        lineRenderer.endColor = endColor;
    }

    public static void LineRendererSetWidth(LineRenderer lineRenderer, float startWidth, float endWidth)
    {
        lineRenderer.startWidth = startWidth;
        lineRenderer.endWidth = endWidth;
    }
}