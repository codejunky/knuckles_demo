﻿using System.Collections;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class SheepShooterManager : MonoBehaviour
{
    public float timeLimit = 30;
    public float randomDistRange = 0.5f;
    public float randomHeightRange = 1;
    public float timeBetweenSpawns = 1;
    public bool restartGameAfterFinished = true;
    
    public UnityEvent OnGameStarted;
    public UnityEvent OnGameFinished;

    public TextMeshPro timerText;
    public TextMeshPro scoreText;
    public GameObject sheepObject;
    public Transform spawnPointRight;
    public Transform spawnPointLeft;
    public CustomButton startButton;


    private float timeLastSpawn = 0;

    private int sheepsDestroyedCounter = 0;
    private int sheepsSpawned = 0;

    internal int finalScore = 0;


    void Start()
    {
        sheepObject.GetComponent<SheepController>().sheepShooterManager = this;
    }

    public void StartGame()
    {
        startButton.activated = false;
        StartCoroutine(GameCoroutine());
    }

    private IEnumerator GameCoroutine()
    {
        print("start game");
        OnGameStarted.Invoke();
        
        //reset game variables
        timeLastSpawn = 0;
        sheepsDestroyedCounter = 0;
        sheepsSpawned = 0;
        
        scoreText.gameObject.SetActive(false);
        timerText.gameObject.SetActive(true);
        
        var remainingTime = timeLimit;
        while (remainingTime > 0)
        {
            //display remaining time
            timerText.text = ((int)remainingTime).ToString("D2");
            
            if (Time.time - timeLastSpawn > timeBetweenSpawns)
            {
                SpawnSheep();
            }

            remainingTime -= Time.deltaTime;
            yield return null;
        }
        
        //disable timer and destroy all remaining sheep
        timerText.gameObject.SetActive(false);
        DestroyRemainingSheep();
        
        //display score
        scoreText.gameObject.SetActive(true);
        finalScore = sheepsDestroyedCounter;
        scoreText.text = "Score:\n" + finalScore.ToString("D3");

        //reset hand to normal (remove weapon) and reset trigger
        if(restartGameAfterFinished) startButton.activated = true;
        
        OnGameFinished.Invoke();
    }

    public void DestroyRemainingSheep()
    {
        var remainingSheep = spawnPointLeft.GetComponentsInChildren<SheepController>().ToList();
        remainingSheep.AddRange(spawnPointRight.GetComponentsInChildren<SheepController>());

        foreach (var sheep in remainingSheep)
        {
            Destroy(sheep.gameObject);
        }
    }

    void SpawnSheep()
    {
        var randomPos = new Vector3(Random.Range(-randomDistRange, randomDistRange),
            Random.Range(randomHeightRange, -randomHeightRange), 0);

        GameObject sheepObjectInst;
        if (sheepsSpawned % 2 == 0)
        {
            sheepObjectInst = Instantiate(sheepObject, spawnPointRight.transform);
            sheepObjectInst.transform.position = spawnPointRight.position + randomPos;
        }
        else
        {
            sheepObjectInst = Instantiate(sheepObject, spawnPointLeft.transform);
            sheepObjectInst.transform.position = spawnPointLeft.position + randomPos;
            sheepObjectInst.transform.Rotate(new Vector3(0, 180, 0), Space.Self);
        }

        PlaySheepSound(sheepObjectInst);


        timeLastSpawn = Time.time;
        sheepsSpawned++;
    }

    private void PlaySheepSound(GameObject sheep)
    {
        var audioSource = sheep.AddComponent<AudioSource>(); //todo: if every sheep has an audiosource, maybe add this in prefab instead
        audioSource.spatialBlend = 1;
        audioSource.pitch = Random.Range(1f, 2f);
        audioSource.clip = GameManager.instance.audioGlobal.sheep_bark;
        audioSource.Play();
    }

    public void OnSheepDestroyed()
    {
        Debug.Log("Sheep destroyed!!!");
        sheepsDestroyedCounter++;
    }
}