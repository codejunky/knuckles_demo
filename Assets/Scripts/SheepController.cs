﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SheepController : MonoBehaviour
{
    public SheepShooterManager sheepShooterManager; //set at runtime
    public GameObject bloodExplosion;
    public float moveSpeed = 0.5f;

    private float lifespan = 7;
    public int bloodParticlesCount = 30;

    private float timeOfBirth;

    // Use this for initialization
    void Start()
    {
        timeOfBirth = Time.time;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(0, 0, -moveSpeed * Time.fixedDeltaTime, Space.Self);

        if (Time.time - timeOfBirth > lifespan)
        {
            Destroy(gameObject);
        }
    }


    public void OnHit()
    {
        var go = Instantiate(bloodExplosion, transform.position, transform.rotation);
        var bloodParticles = go.GetComponent<ParticleSystem>();
        var audioSource = go.GetComponent<AudioSource>();
        
        //explosion sound
        audioSource.pitch = Random.Range(0.5f, 1.5f);
        audioSource.clip = GameManager.instance.audioGlobal.sheep_explosion;
        audioSource.Play();
        //explosion particles
        bloodParticles.Play();
        
        Destroy(gameObject);
        sheepShooterManager.OnSheepDestroyed();
    }
}