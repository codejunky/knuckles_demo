﻿using System;
using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.Events;

public class Mouth : MonoBehaviour
{	
	public int eatingParticleCount = 50;
	
	private ParticleSystem particles;
	private AudioSource audioSource;
	private AudioClip eatingSound;

	public Action<Eatable> OnEating;
	
	// Use this for initialization
	private void Start()
	{
		particles = GetComponent<ParticleSystem>();
		audioSource = GetComponent<AudioSource>();
		eatingSound = GameManager.instance.audioGlobal.eating_chew;
	}

	private void OnTriggerEnter(Collider other)
	{
		var eatable = other.attachedRigidbody.GetComponent<Eatable>();
		if (eatable != null)
		{
			eatable.Eat();
			Eat();
			if(OnEating != null)
				OnEating.Invoke(eatable);
		}
	}

//	void Update () {
//		if (Input.GetKeyDown("h"))
//		{
//			Eat();
//		}
//	}

	private void Eat()
	{
		particles.Emit(eatingParticleCount);
		audioSource.clip = eatingSound;
		audioSource.Play();
	}
}
