﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class CustomInteractable : Interactable
{
    public event Action onHoverBegin;
    private CustomHand attachedHand;
    public virtual void BeginInteraction(CustomHand hand)
    {
        base.OnAttachedToHand(hand);
        attachedHand = hand;
    }

    protected override void OnHandHoverBegin(Hand hand)
    {
        base.OnHandHoverBegin(hand);
        onHoverBegin?.Invoke();
    }

    public virtual void EndInteraction(CustomHand hand)
    {
        base.OnDetachedFromHand(hand);
        attachedHand = null;
    }
    
    public virtual void ForceDetach(CustomHand hand = null)
    {
        if (hand == null) hand = attachedHand;
        throw new System.NotImplementedException();

    }


}
