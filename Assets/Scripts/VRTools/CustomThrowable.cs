﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class CustomThrowable : Throwable
{
    [Header("Custom Settings:")] public bool overwriteAttachmentOffset;
    public Transform attachmentOffsetLeft;
    public Transform attachmentOffsetRight;
    public bool canBeShredded;
    public bool breaksShredder;

    public void Shred()
    {
        if(canBeShredded)
            Destroy(gameObject);
    }

    protected override void HandHoverUpdate(Hand hand)
    {
        GrabTypes startingGrabType = hand.GetGrabStarting();
            
        if (startingGrabType != GrabTypes.None)
        {
            if (overwriteAttachmentOffset)
            {
                attachmentOffset = hand.handType == SteamVR_Input_Sources.LeftHand
                    ? attachmentOffsetLeft
                    : attachmentOffsetRight;
            }
            
            hand.AttachObject( gameObject, startingGrabType, attachmentFlags, attachmentOffset );
            hand.HideGrabHint();
        }
    }

    protected override void HandAttachedUpdate(Hand hand)
    {
        base.HandAttachedUpdate(hand);
        //todo: check if hand has certain distance to attachpoint and detach if too far away
    }
}